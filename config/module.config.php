<?php

namespace Bitkorn\Mail;

use Bitkorn\Mail\Factory\Draft\DraftManagerFactory;
use Bitkorn\Mail\Factory\MailWrapperFactory;
use Bitkorn\Mail\Factory\Service\MailServiceFactory;
use Bitkorn\Mail\Factory\SimpleMailerFactory;
use Bitkorn\Mail\Factory\Table\MailSendAttachTableFactory;
use Bitkorn\Mail\Factory\Table\MailSendTableFactory;
use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Mail\SimpleMailer;
use Bitkorn\Mail\Mail\Draft\DraftManager;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Mail\Table\MailSendAttachTable;
use Bitkorn\Mail\Table\MailSendTable;

return [
    'service_manager' => [
        'abstract_factories' => [],
        'factories'          => [
            MailWrapper::class         => MailWrapperFactory::class,
            SimpleMailer::class        => SimpleMailerFactory::class,
            DraftManager::class        => DraftManagerFactory::class,
            MailService::class         => MailServiceFactory::class,
            // table
            MailSendTable::class       => MailSendTableFactory::class,
            MailSendAttachTable::class => MailSendAttachTableFactory::class,
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map'        => [
            'draft/default' => realpath(__DIR__ . '/../view/draft/default.phtml'),
        ],
    ],
    'bitkorn_mail'    => [
        /**
         * https://docs.laminas.dev/laminas-mail/transport/smtp-options
         */
        'is_smtp'              => true,
        'smtp_from_servername' => 'bitkorn.de', // real network Host name
        'smtp_host'            => 'bitkorn.de', // defaults to "127.0.0.1"
        'smtp_port'            => 25, // defaults to "25": 25, 465 (SSL/TLS), 587 (MSA/STARTTLS)
        'connection_class'     => 'login', // plain | login | crammd5 (\Laminas\Mail\Protocol\Smtp\Auth\*) ...NTLM !?!?!?
        'smtp_user'            => 'mail@bitkorn.de',
        'smtp_passwd'          => 'mysecret',
        'ssl'                  => null, // ssl | tls
        'admin_mail'           => [
            'admin_mail_mailadress_to'   => 'mail@bitkorn.de',
            'admin_mail_name_to'         => 'bitkorn',
            'admin_mail_mailadress_from' => 'mail@bitkorn.de',
            'admin_mail_name_from'       => 'admin'
        ],
        'tmp_folder'           => __DIR__ . '/../../../../data/tmp'
    ],
];
