```shell script
composer require laminas/laminas-mail
composer require laminas/laminas-mime
composer require laminas/laminas-crypt
```

Folder ```/data/tmp``` must exist and must be writable for the webserver user.

html2text for plaintext Emails:
```shell script
sudo apt install html2text
```
