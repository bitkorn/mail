<?php

namespace Bitkorn\Mail\Entity;

use Laminas\Http\PhpEnvironment\Request;
use Laminas\Stdlib\ArrayUtils;

class MailEntity
{
    protected \HTMLPurifier $htmlPurifier;
    protected string $fromEmail = '';
    protected string $fromName = '';
    protected string $toEmail = '';
    protected string $toName = '';
    protected string $subject = '';
    protected string $contentText = '';
    protected string $contentHtml = '';
    protected array $ccs = [];
    protected array $bccs = [];
    protected array $attachments = [];

    protected array $args = [
        'from_email'   => FILTER_SANITIZE_EMAIL,
        'from_name'    => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH],
        'to_email'     => FILTER_SANITIZE_EMAIL,
        'to_name'      => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH],
        'subject'      => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH],
        'content_text' => [
            'filter' => FILTER_SANITIZE_SPECIAL_CHARS,
            'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH],
        'content_html' => [
            'filter' => FILTER_UNSAFE_RAW,
            'flags'  => FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH],
        'ccs'          => [
            'filter' => FILTER_SANITIZE_EMAIL,
            'flags'  => FILTER_REQUIRE_ARRAY,
        ],
        'bccs'         => [
            'filter' => FILTER_SANITIZE_EMAIL,
            'flags'  => FILTER_REQUIRE_ARRAY,
        ],
    ];

    /**
     * MailEntity constructor.
     * @param \HTMLPurifier $htmlPurifier
     */
    public function __construct(\HTMLPurifier $htmlPurifier)
    {
        $this->htmlPurifier = $htmlPurifier;
        $this->htmlPurifier->config->set('URI.AllowedSchemes', ['data' => true]); // <img src="data:image/png;base64
    }

    public function handleHttpPost(): void
    {
        $input = filter_input_array(INPUT_POST, $this->args);
        if (isset($input['from_email'])) {
            $this->setFromEmail($input['from_email']);
        }
        if (isset($input['from_name'])) {
            $this->setFromName($input['from_name']);
        }
        if (isset($input['to_email'])) {
            $this->setToEmail($input['to_email']);
        }
        if (isset($input['to_name'])) {
            $this->setToName($input['to_name']);
        }
        if (isset($input['subject'])) {
            $this->setSubject($input['subject']);
        }
        if (isset($input['content_text'])) {
            $this->setContentText($input['content_text']);
        }
        if (isset($input['content_html'])) {
            $this->setContentHtml($this->htmlPurifier->purify($input['content_html']));
        }

        if (isset($input['ccs']) && is_array($input['ccs']) && !empty($input['ccs'])) {
            $this->setCcs($input['ccs']);
        }
        if (isset($input['bccs']) && is_array($input['bccs']) && !empty($input['bccs'])) {
            $this->setBccs($input['bccs']);
        }
    }

    public function handleHttpPostFiles(Request $request): bool
    {
        $attachments = $request->getFiles()->toArray();
        $attachments = $attachments['attachments'] ?? [];
        foreach ($attachments as $attachment) {
            $this->addAttachment($attachment['tmp_name'], $attachment['type']);
        }
    }

    /**
     * @return string
     */
    public function getFromEmail(): string
    {
        return $this->fromEmail;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail(string $fromEmail): void
    {
        $this->fromEmail = $fromEmail;
    }

    /**
     * @return string
     */
    public function getFromName(): string
    {
        return $this->fromName;
    }

    /**
     * @param string $fromName
     */
    public function setFromName(string $fromName): void
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getToEmail(): string
    {
        return $this->toEmail;
    }

    /**
     * @param string $toEmail
     */
    public function setToEmail(string $toEmail): void
    {
        $this->toEmail = $toEmail;
    }

    /**
     * @return string
     */
    public function getToName(): string
    {
        return $this->toName;
    }

    /**
     * @param string $toName
     */
    public function setToName(string $toName): void
    {
        $this->toName = $toName;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getContentText(): string
    {
        return $this->contentText;
    }

    /**
     * @param string $contentText
     */
    public function setContentText(string $contentText): void
    {
        $this->contentText = $contentText;
    }

    /**
     * @return string
     */
    public function getContentHtml(): string
    {
        return $this->contentHtml;
    }

    /**
     * @param string $contentHtml
     */
    public function setContentHtml(string $contentHtml): void
    {
        $this->contentHtml = $contentHtml;
    }

    /**
     * @return array Zero indexed array with email addresses.
     */
    public function getCcs(): array
    {
        return $this->ccs;
    }

    /**
     * @param array $ccs Zero indexed array with email addresses.
     */
    public function setCcs(array $ccs): void
    {
        $this->ccs = $ccs;
    }

    public function addCc(string $cc): void
    {
        $this->ccs[] = $cc;
    }

    /**
     * @return array Zero indexed array with email addresses.
     */
    public function getBccs(): array
    {
        return $this->bccs;
    }

    /**
     * @param array $bccs Zero indexed array with email addresses.
     */
    public function setBccs(array $bccs): void
    {
        $this->bccs = $bccs;
    }

    public function addBcc(string $bcc): void
    {
        $this->bccs[] = $bcc;
    }

    public function getRecipients(): array
    {
        return ArrayUtils::merge([$this->toEmail], $this->ccs, $this->bccs);
    }

    /**
     * @return array An assoc array: key=filepath; value=mimeType
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments An assoc array: key=filepath; value=mimeType
     */
    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * @param string $filepath Absolute file path.
     * @param string $mimeType E.g. 'application/pdf' or 'image/jpg'.
     */
    public function addAttachment(string $filepath, string $mimeType): void
    {
        $this->attachments[$filepath] = $mimeType;
    }
}
