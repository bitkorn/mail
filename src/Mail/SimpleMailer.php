<?php

namespace Bitkorn\Mail\Mail;

use Laminas\Log\Logger;

/**
 * Class to send Emails simple
 */
class SimpleMailer
{
    protected Logger $logger;
    private MailWrapper $mailWrapper;
    private array $config;
    protected string $message = '';

    public function getMessage(): string
    {
        return $this->message;
    }

    public function __construct(MailWrapper $mailWrapper, array $config, Logger $logger)
    {
        $this->mailWrapper = $mailWrapper;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param string $mailText It is set as plain text and HTML email content.
     * @param string $emailadress
     * @param string $subject
     * @return bool
     */
    public function sendAdminEmail(string $mailText, string $emailadress = '', string $subject = ''): bool
    {
        $this->mailWrapper->setTextHtml($mailText);
        $this->mailWrapper->setTextPlain($mailText);
        $this->mailWrapper->setFromEmail($this->config['admin_mail']['admin_mail_mailadress_from']);
        $this->mailWrapper->setFromName($this->config['admin_mail']['admin_mail_name_from']);
        if (empty($emailadress)) {
            $this->mailWrapper->setToEmail($this->config['admin_mail']['admin_mail_mailadress_to']);
            $this->mailWrapper->setToName($this->config['admin_mail']['admin_mail_name_to']);
        } else {
            $this->mailWrapper->setToEmail($emailadress);
            $this->mailWrapper->setToName($emailadress);
        }
        if (empty($subject)) {
            $this->mailWrapper->setSubject('Admin Email');
        } else {
            $this->mailWrapper->setSubject($subject);
        }

        $success = false;
        try {
            $success = $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            $this->logger->err($e->getMessage());
        }
        return $success;
    }

    /**
     * @param string $emailTo
     * @param string $subject
     * @param string $mailText It is set as plain text and HTML email content.
     * @return bool
     */
    public function sendEmailTo(string $emailTo, string $subject, string $mailText): bool
    {
        $this->mailWrapper->setTextHtml($mailText);
        $this->mailWrapper->setTextPlain($mailText);
        $this->mailWrapper->setFromEmail($this->config['admin_mail']['admin_mail_mailadress_from']);
        $this->mailWrapper->setFromName($this->config['admin_mail']['admin_mail_name_from']);

        $this->mailWrapper->setToEmail($emailTo);
        $this->mailWrapper->setToName($emailTo);

        $this->mailWrapper->setSubject($subject);

        $success = false;
        try {
            $success = $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $e) {
            $this->logger->err($e->getMessage());
            $this->message = $this->mailWrapper->getMessage();
        }
        return $success;
    }
}
