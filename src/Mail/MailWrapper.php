<?php

namespace Bitkorn\Mail\Mail;

use Laminas\Log\Logger;
use Laminas\Mail\Transport\Sendmail;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Laminas\Mime\Part;
use Laminas\Mime\Mime;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mail\Message as MailMessage;
use Laminas\Stdlib\ArrayUtils;

class MailWrapper
{
    protected Logger$logger;
    protected string $message = '';
    protected array $mailConfig;
    protected string $fromName;
    protected string $fromEmail;
    protected string $toName;
    protected string $toEmail;
    protected array $ccEmails = [];
    protected array $bccEmails = [];
    protected string $subject;
    protected string $textPlain;
    protected string $textHtml;
    protected array $attachments = [];

    protected bool $onlyHtml = false;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function resetMailContent()
    {
        $this->textPlain = '';
        $this->textHtml = '';
    }

    public function resetMailRecipient()
    {
        $this->resetMailContent();
        $this->toName = '';
        $this->toEmail = '';
        $this->ccEmails = [];
        $this->bccEmails = [];
    }

    public function resetMailAll()
    {
        $this->resetMailContent();
        $this->toName = '';
        $this->toEmail = '';
        $this->fromName = '';
        $this->fromEmail = '';
        $this->ccEmails = [];
        $this->bccEmails = [];
        $this->subject = '';
        $this->attachments = [];
    }

    /**
     * @param array $mailConfig The config from module.config.php['bitkorn_mail'].
     */
    public function setMailConfig(array $mailConfig)
    {
        $this->mailConfig = $mailConfig;
    }

    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    public function setFromNameFromConfig()
    {
        $this->fromName = $this->mailConfig['admin_mail']['admin_mail_name_from'];
    }

    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    public function setFromEmailFromConfig()
    {
        $this->fromEmail = $this->mailConfig['admin_mail']['admin_mail_mailadress_from'];
    }

    public function setToName($toName)
    {
        $this->toName = $toName;
    }

    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;
    }

    public function setCcEmails(array $ccEmails): void
    {
        $this->ccEmails = $ccEmails;
    }

    public function setBccEmails(array $bccEmails)
    {
        $this->bccEmails = $bccEmails;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function setTextPlain($textPlain)
    {
        $this->textPlain = str_replace('<br>', "\r\n", $textPlain);
    }

    public function setTextHtml($textHtml)
    {
        $this->textHtml = $textHtml;
    }

    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    public function setOnlyHtml(bool $onlyHtml): void
    {
        $this->onlyHtml = $onlyHtml;
    }

    protected function makeMailParts(): array
    {
        if (empty($this->mailConfig)) {
            return [];
        }
        if (!$this->textHtml && $this->onlyHtml) {
            throw new \InvalidArgumentException('None textHtml supplied, but onlyHTML is true.');
        }
        $parts = [];
        if (!empty($this->textPlain)) {
            $textPart = new Part($this->textPlain);
            $textPart->type = Mime::TYPE_TEXT;
            $textPart->charset = 'utf-8';
            $parts[] = $textPart;
        } else if (!empty($this->textHtml) && !$this->onlyHtml) {
            $this->textPlain = $this->html2text($this->textHtml);
            $textPart = new Part($this->textPlain);
            $textPart->type = Mime::TYPE_TEXT;
            $textPart->charset = 'utf-8';
            $parts[] = $textPart;
        }

        if (!empty($this->textHtml)) {
            $htmlPart = new Part($this->textHtml);
            $htmlPart->type = Mime::TYPE_HTML;
            $htmlPart->charset = 'utf-8';
            $parts[] = $htmlPart;
        }
        return $parts;
    }

    protected function makeMailMessage(MimeMessage $mimeMessage): MailMessage
    {
        $mail = new MailMessage();
        $mail->setEncoding('utf-8');
        $mail->setFrom($this->fromEmail, $this->fromName);
        $mail->addTo($this->toEmail, $this->toName);
        if (!empty($this->bccEmails)) {
            foreach ($this->bccEmails as $bccEmail) {
                $mail->addBcc($bccEmail, $bccEmail);
            }
        }
        if (!empty($this->ccEmails)) {
            foreach ($this->ccEmails as $ccEmail) {
                $mail->addCc($ccEmail, $ccEmail);
            }
        }
        $mail->setSubject($this->subject);
        $mail->setBody($mimeMessage);
        return $mail;
    }

    protected function computeMimeType(MailMessage $mail): void
    {
        // If it has two parts (HTML + plain), we have to force the correct MIME
        // alternative allows the MUA to choose between HTML and plain text
        // By default it sets multipart/mixed, which makes e.g. thunderbird display both
        if (empty($this->attachments) && $this->textPlain && ($this->textHtml && !$this->onlyHtml)) {
            $mail->getHeaders()->get('content-type')->setType('multipart/alternative');
        } else if (!empty($this->attachments)) {
            $mail->getHeaders()->get('content-type')->setType('multipart/related');
        }
    }

    /**
     * @return int 1 on success, -1 on error
     */
    public function sendMail(): int
    {
        if (empty($parts = $this->makeMailParts())) {
            return -1;
        }
        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts($parts);
        $mail = $this->makeMailMessage($mimeMessage);
        $this->computeMimeType($mail);
        return $this->send($mail);
    }

    /**
     *
     * @param array $attachments e.g.: [['/home/username/Documents/document.pdf' => 'application/pdf'], ['/home/username/Images/me.jpg' => 'image/jpg']]
     * @return int 1 on success
     */
    public function sendMailWithAttachment(array $attachments = []): int
    {
        if (empty($parts = $this->makeMailParts())) {
            return -1;
        }
        $content = new MimeMessage();
        $content->setParts($parts);

        $contentPart = new Part($content->generateMessage());
        $contentPart->type = Mime::MULTIPART_ALTERNATIVE . ';' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';

        $this->attachments = ArrayUtils::merge($this->attachments, $attachments);
        $attachmentParts = [];
        foreach ($this->attachments as $filePath => $type) {
            if (!file_exists($filePath)) {
                throw new \RuntimeException('$filePath: ' . $filePath . ' does not exist or can not read');
            }
            $fileContent = fopen($filePath, 'r');
            $attachment = new Part($fileContent);
            $attachment->type = $type;
            $filePathExpl = explode(DIRECTORY_SEPARATOR, $filePath);
            $attachment->filename = array_pop($filePathExpl);
            $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
            $attachment->encoding = Mime::ENCODING_BASE64;
            $attachmentParts[] = $attachment;
        }

        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts(array_merge([$contentPart], $attachmentParts));

        $mail = $this->makeMailMessage($mimeMessage);
        $this->computeMimeType($mail);

        return $this->send($mail);
    }

    /**
     *
     * @param MailMessage $mail
     * @return int
     */
    private function send(MailMessage $mail): int
    {
        if ($this->mailConfig['is_smtp']) {
            $transport = new Smtp();
            $optionArray = [
                'name' => $this->mailConfig['smtp_from_servername'],
                'host' => $this->mailConfig['smtp_host'],
                'port' => $this->mailConfig['smtp_port']
            ];
            if (!empty($this->mailConfig['smtp_user'])) {
                $optionArray = array_merge($optionArray,
                    [
                        'connection_class' => $this->mailConfig['connection_class'],
                        'connection_config' => [
                            'username' => $this->mailConfig['smtp_user'],
                            'password' => $this->mailConfig['smtp_passwd'],
                            'ssl' => $this->mailConfig['ssl']
                        ]
                    ]);
            }
            $smtpOptions = new SmtpOptions($optionArray);
            $transport->setOptions($smtpOptions);
            try {
                $transport->send($mail);
            } catch (\RuntimeException $ex) {
                $this->logger->err($ex->getMessage());
                $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
                $this->logger->err('host: ' . $this->mailConfig['smtp_host']);
                return -1;
            }
        } else {
            $transport = new Sendmail();
            try {
                $transport->send($mail);
            } catch (\RuntimeException $ex) {
                $this->message = $ex->getMessage();
                $this->logger->err($ex->getMessage());
                $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
                return -1;
            }
        }
        return 1;
    }

    /**
     * @param string $textHtml HTML String
     * @return string Plain Text String
     * @throws \RuntimeException
     */
    private function html2text(string $textHtml): string
    {
        if (strlen(exec('whereis html2text')) < 15) {
            throw new \RuntimeException('There is no html2text installed');
        }
        if (!isset($this->mailConfig['tmp_folder'])) {
            throw new \RuntimeException(__NAMESPACE__ . ' tmp_folder is not set.');
        }
        if (!file_exists($this->mailConfig['tmp_folder'])) {
            throw new \RuntimeException('Folder does not exist: ' . $this->mailConfig['tmp_folder']);
        }
        $now = time();
        $fileIn = $this->mailConfig['tmp_folder'] . '/html2text' . $now;
        $fileOut = $this->mailConfig['tmp_folder'] . '/html2textout' . $now;
        file_put_contents($fileIn, $textHtml);
        exec('html2text ' . $fileIn . ' >> ' . $fileOut);
        $text = file_get_contents($fileOut);
        unlink($fileIn);
        unlink($fileOut);
        return $text;
    }
}
