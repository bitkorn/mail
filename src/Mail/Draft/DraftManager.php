<?php

namespace Bitkorn\Mail\Mail\Draft;

use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;

class DraftManager
{
    protected Logger $logger;
    const DEFAULT_DRAFT_TEMPLATE = 'draft/default';
    private PhpRenderer $renderer;
    private string $template;
    private string $content = '';
    private array $viewVariables = [];

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setRenderer(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function setTemplate(string $template)
    {
        $this->template = $template;
    }

    /**
     * @param string $content The content for the email body.
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @param string $key View variable name.
     * @param string $value View variable value.
     */
    public function setViewVariable(string $key, string $value)
    {
        $this->viewVariables[$key] = $value;
    }

    /**
     * @param array $viewVariables An array with key=viewVariable name; value=viewVariable value
     */
    public function setViewVariables(array $viewVariables)
    {
        $this->viewVariables = array_merge($this->viewVariables, $viewVariables);
    }

    /**
     * Render the template with his view variables.
     * @return string The HTML output.
     */
    public function render(): string
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('content', $this->content);

        if (isset($this->template)) {
            $viewModel->setTemplate($this->template);
        } else {
            $viewModel->setTemplate(self::DEFAULT_DRAFT_TEMPLATE);
        }
        if (!empty($this->viewVariables)) {
            $viewModel->setVariables($this->viewVariables);
        }
        return $this->renderer->render($viewModel);
    }

}
