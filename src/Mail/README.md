https://framework.zend.com/manual/2.4/en/modules/zend.mail.smtp.options.html

Laminas\Mail\Transport\SmtpOptions
Overview
This document details the various options available to the Laminas\Mail\Transport\Smtp mail transport.
Quick Start
Basic SMTP Transport Usage:

use Laminas\Mail\Transport\Smtp as SmtpTransport;
use Laminas\Mail\Transport\SmtpOptions;

```php
    // Setup SMTP transport

    $transport = new SmtpTransport();

    $options   = new SmtpOptions(array(

        'name' => 'localhost.localdomain',

        'host' => '127.0.0.1',

        'port' => 25,

    ));

    $transport->setOptions($options);
```