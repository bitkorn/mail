<?php

namespace Bitkorn\Mail\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class MailSendAttachTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send_attach';

    /**
     * @param string $mailSendUuid
     * @return array
     */
    public function getMailSendAttachs(string $mailSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['mail_send_uuid' => $mailSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $fileUuids
     * @param string $mailSendUuid
     * @return int
     */
    public function insertMailSendAttachs(array $fileUuids, string $mailSendUuid): int
    {
        if (empty($fileUuids) || !is_array($fileUuids)) {
            return '';
        }
        $insert = $this->sql->insert();
        try {
            $successCount = 0;
            foreach ($fileUuids as $fileUuid) {
                $insert->values([
                    'mail_send_attach_uuid' => $this->uuid(),
                    'mail_send_uuid'        => $mailSendUuid,
                    'file_uuid'             => $fileUuid,
                ]);
                if ($this->insertWith($insert) > 0) {
                    $successCount++;
                }
            }
            return $successCount;
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
