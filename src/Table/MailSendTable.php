<?php

namespace Bitkorn\Mail\Table;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

class MailSendTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'mail_send';

    /**
     * @param string $mailSendUuid
     * @return array
     */
    public function getMailSend(string $mailSendUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['mail_send_uuid' => $mailSendUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param MailEntity $mailEntity
     * @param string $userUuid
     * @return string
     */
    public function insertMailSend(MailEntity $mailEntity, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'mail_send_uuid'         => $uuid,
                'mail_send_from_name'    => $mailEntity->getFromName(),
                'mail_send_from_email'   => $mailEntity->getFromEmail(),
                'mail_send_to_name'      => $mailEntity->getToName(),
                'mail_send_to_email'     => $mailEntity->getToEmail(),
                'mail_send_cc_csv'       => implode(',', $mailEntity->getCcs()),
                'mail_send_bcc_csv'      => implode(',', $mailEntity->getBccs()),
                'mail_send_subject'      => $mailEntity->getSubject(),
                'mail_send_content_text' => $mailEntity->getContentText(),
                'mail_send_content_html' => $mailEntity->getContentHtml(),
                'user_uuid'              => $userUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
