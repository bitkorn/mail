<?php

namespace Bitkorn\Mail\Service;

use Bitkorn\Mail\Entity\MailEntity;
use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Table\MailSendAttachTable;
use Bitkorn\Mail\Table\MailSendTable;
use Bitkorn\Trinket\Service\AbstractService;

class MailService extends AbstractService
{
    protected MailWrapper $mailWrapper;
    protected MailSendTable $mailSendTable;
    protected MailSendAttachTable $mailSendAttachTable;

    public function setMailWrapper(MailWrapper $mailWrapper): void
    {
        $this->mailWrapper = $mailWrapper;
    }

    public function setMailSendTable(MailSendTable $mailSendTable): void
    {
        $this->mailSendTable = $mailSendTable;
    }

    public function setMailSendAttachTable(MailSendAttachTable $mailSendAttachTable): void
    {
        $this->mailSendAttachTable = $mailSendAttachTable;
    }

    /**
     * Save Email data (db.mail_send & db.mail_send_attach).
     * It uses no database transaction. If it returns FALSE, you may have to delete saved data manually.
     * @param MailEntity $mailEntity
     * @param array $fileUuids
     * @param string $userUuid
     * @return string The new mail_send_uuid or an empty string on error.
     */
    public function saveMail(MailEntity $mailEntity, array $fileUuids, string $userUuid): string
    {
        if (
            empty($mailSendUuid = $this->mailSendTable->insertMailSend($mailEntity, $userUuid))
            || (!empty($fileUuids) && $this->mailSendAttachTable->insertMailSendAttachs($fileUuids, $mailSendUuid) != count($fileUuids))
        ) {
            return '';
        }
        return $mailSendUuid;
    }

    /**
     * @param MailEntity $mailEntity
     * @return bool
     */
    public function sendMail(MailEntity $mailEntity): bool
    {
        $this->mailWrapper->setFromName($mailEntity->getFromName());
        $this->mailWrapper->setFromEmail($mailEntity->getFromEmail());
        $this->mailWrapper->setToName($mailEntity->getToName());
        $this->mailWrapper->setToEmail($mailEntity->getToEmail());
        $this->mailWrapper->setCcEmails($mailEntity->getCcs());
        $this->mailWrapper->setBccEmails($mailEntity->getBccs());
        $this->mailWrapper->setSubject($mailEntity->getSubject());
        $contentText = $mailEntity->getContentText();
        $contentHtml = $mailEntity->getContentHtml();
        if ($contentText && $contentHtml) {
            $this->mailWrapper->setTextPlain($contentText);
            $this->mailWrapper->setTextHtml($contentHtml);
        } else if ($contentText) {
            $this->mailWrapper->setTextPlain($contentText);
        } else {
            $this->mailWrapper->setTextHtml($contentHtml);
        }
        if (!empty($attachs = $mailEntity->getAttachments())) {
            $this->mailWrapper->setAttachments($attachs);
            return $this->mailWrapper->sendMailWithAttachment();
        } else {
            return $this->mailWrapper->sendMail();
        }
    }

}
