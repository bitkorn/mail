<?php

namespace Bitkorn\Mail\Factory\Draft;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Bitkorn\Mail\Mail\Draft\DraftManager;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplateMapResolver;

class DraftManagerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $draftManager = new DraftManager();
        $draftManager->setLogger($container->get('logger'));
        $renderer = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap($container->get('ViewTemplateMapResolver'));
        $renderer->setResolver($resolver);
        /** @var HelperPluginManager $helperPluginManager */
        $helperPluginManager = $container->get('ViewHelperManager');
        $renderer->setHelperPluginManager($helperPluginManager);
        $draftManager->setRenderer($renderer);
        return $draftManager;
    }
}
