<?php

namespace Bitkorn\Mail\Factory\Service;

use Bitkorn\Mail\Mail\MailWrapper;
use Bitkorn\Mail\Service\MailService;
use Bitkorn\Mail\Table\MailSendAttachTable;
use Bitkorn\Mail\Table\MailSendTable;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class MailServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException If an exception is raised when creating a service.
     * @throws ContainerException If any other error occurs.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new MailService();
        $service->setLogger($container->get('logger'));
        $service->setMailWrapper($container->get(MailWrapper::class));
        $service->setMailSendTable($container->get(MailSendTable::class));
        $service->setMailSendAttachTable($container->get(MailSendAttachTable::class));
        return $service;
    }
}
