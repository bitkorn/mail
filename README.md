# Laminas Module bitkorn/mail

uses the following Laminas modules:
- [laminas-mail](https://docs.laminas.dev/laminas-mail/)
- [laminas-mime](https://docs.laminas.dev/laminas-mime/)

uses the following bitkorn modules:
- bitkorn/trinket
- bitkorn/files
